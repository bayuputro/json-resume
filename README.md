# JSON Resume #

### Demo ###
* Frontend: http://bayuputra.info/portfolio/resume/frontend/
* Backend: http://bayuputra.info/portfolio/resume/backend/

### Libraries ###
* jQuery version 2.0.0

### Description ###
* I build the backend framework from scratch using MVC approach, using Controller as a mapping router of URL request.
* The application is build using RIA (Rich Internet Application) architecture with frontend and backend separated.