<?php
    /**
    * BayuController.php
    * 
    * 
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package 
    */
    namespace controllers; 

    use system\classes\BaseController;
    use models\Resume;
    use models\Profile;
    use models\Experience;
    use models\Education;
    use models\Language;
    use models\Skill;

    class BayuController extends BaseController {

        private $resume;

        public function index(){
            $this->resume = new Resume();

            $this->_set_profile();
            $this->_add_experiences();
            $this->_add_educations();
            $this->_add_languages();
            $this->_add_skills();

            $this->resume->printJson();


        }

        private function _set_profile(){
            $Profile = new Profile('Bayu Putra');
            $Profile->setField('photo_url','../frontend/images/me.jpg');
            $Profile->setField('current_role','Head Of Technologist');
            $Profile->setField('current_company','Mitosis Sdn Bhd');
            $Profile->setField('phone_number','+601127192098');
            $Profile->setField('location','Kuala Lumpur');
            $Profile->setField('expected_salary','RM 13,000');
            $Profile->setField('email','bayuputra_mail@yahoo.com');
            $Profile->setField('summary','I have 8 years experience on the web development and other various platform. My previous jobs as development manager and head of technologist make me have a proven experience of leading and grooming team, good problem solving and communication skill.');

            $this->resume->setProfile($Profile);
        }

        private function _add_experiences(){

            $experience = new Experience();        
            $experience->setField('duration','1 year 3 months');
            $experience->setField('date_range','Oct 2014 - Present');
            $experience->setField('role','Head Of Technologist');
            $experience->setField('company','Mitosis Sdn. Bhd.');
            $experience->setField('industry','Digital Agency');
            $experience->setField('level','Head Of Department');
            $experience->setField('location','Kuala Lumpur, Malaysia');
            $experience->setField('salary','10,000 RM');
            $experience->setField('job_description', [
                'Interact with difference stockholders within the company and external investors',
                'Lead team for overall technical set up from gathering requirement, planning, design the architecture, development, test and implementation',
                'Bring value to the company from the latest technology',
                'Lead, build and manage technology team to ensure the department output can reach KPI',
                'Research the best and latest technology that will be used in company',
                'Determine number of man days and project costing for the technology part'
            ]);
            $this->resume->addExperience($experience);


            $experience = new Experience();
            $experience->setField('duration','1 year 4 months');
            $experience->setField('date_range','Jun 2013 - Oct 2014');
            $experience->setField('role','Technical Lead');
            $experience->setField('company','Mitosis Sdn. Bhd.');
            $experience->setField('industry','Digital Agency');
            $experience->setField('level','Manager');
            $experience->setField('location','Kuala Lumpur, Malaysia');
            $experience->setField('salary','MYR 5,000');
            $experience->setField('job_description',[
                'Ensuring every project running smoothly in term of technical part',
                'Building framework for all programmers in projects',
                'Research & determine technologies that company will use in future',
            ]);
            $this->resume->addExperience($experience);


            $experience = new Experience();
            $experience->setField('duration','1 year 5 months');
            $experience->setField('date_range','Jun 2013 - Oct 2014');
            $experience->setField('role','General Manager Development');
            $experience->setField('company','PT.Altermyth');
            $experience->setField('industry','Advertising & Game Development');
            $experience->setField('level','Manager');
            $experience->setField('location','Jakarta Raya, Indonesia');
            $experience->setField('salary','IDR 10,000,000');
            $experience->setField('job_description',[
                'Ensuring development progress running well',
                'Building SOP for all software development in company',
                'Calculating development cost and man power for projects',
                'Communicate with clients to give best IT solution',
                'Manage resource (man power) traffic and allocation',
            ]);
            $this->resume->addExperience($experience);


            $experience = new Experience();
            $experience->setField('duration','1 year 6 months');
            $experience->setField('date_range','Jun 2009 - Dec 2010');
            $experience->setField('role','Software Enginer');
            $experience->setField('company','Belugerin Games');
            $experience->setField('industry','Game Development');
            $experience->setField('level','Entry Level');
            $experience->setField('location','Jakarta Raya, Indonesia');
            $experience->setField('salary','IDR 4,000,000');
            $experience->setField('job_description',[
                'Developing Belugerin Studios Websites including database design, layout design, art design, programming and deployment of websites',
                'Maintenacing stability dan and security of Belugerinstudios websites',
                'Developing system integration for Belugerinstudios products to websites',
            ]);
            $this->resume->addExperience($experience);

        }

        private function _add_educations(){           
            $education = new Education();
            $education->setField('school_name','Sebelas Maret University');
            $education->setField('graduation_year','2006');
            $education->setField('faculty','Information Technology');
            $this->resume->addEducation(($education));
        }
        
        private function _add_languages(){
            $lang = new Language();
            $lang->setField('language_name','Indonesian');
            $lang->setField('spoken','10');
            $lang->setField('written','10');
            
            $this->resume->addLanguage($lang);
            
            
            $lang = new Language();
            $lang->setField('language_name','English');
            $lang->setField('spoken','8');
            $lang->setField('written','8');
            
            $this->resume->addLanguage($lang);
        }
        
        private function _add_skills(){
            $skill = new Skill();
            $skill->setField('skill_name','PHP');
            $skill->setField('level','10');
            $this->resume->addSkill($skill);
            
            $skill = new Skill();
            $skill->setField('skill_name','MySQL/SQL Query');
            $skill->setField('level','10');
            $this->resume->addSkill($skill);
            
            $skill = new Skill();
            $skill->setField('skill_name','Javascript');
            $skill->setField('level','10');
            $this->resume->addSkill($skill);
            
            $skill = new Skill();
            $skill->setField('skill_name','C#');
            $skill->setField('level','8');
            $this->resume->addSkill($skill);
            
            $skill = new Skill();
            $skill->setField('skill_name','Management & Leadership');
            $skill->setField('level','8');
            $this->resume->addSkill($skill);
            
            $skill = new Skill();
            $skill->setField('skill_name','Python Djanggo');
            $skill->setField('level','7');
            $this->resume->addSkill($skill);
            
            $skill = new Skill();
            $skill->setField('skill_name','Ruby On Rails');
            $skill->setField('level','5');
            $this->resume->addSkill($skill);
           
        }

}