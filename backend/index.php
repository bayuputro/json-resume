<?php

/**
 * index.php
 * 
 * Gateway of the application
 * @author Bayu Putra <bayuputra_mail@yahoo.com>
 * @version 1.0
 * @package 
 */
 
spl_autoload_register(function($name) {
    require_once(str_replace('\\','/',$name).".php");
});

//starting up
include 'system/bootstrap.php';

//loading config file 
system\classes\Config::getInstance()->setFile('config.php');

try{
	init();
}catch(system\classes\ControllerNotExistException $e){
	echo $e->getMessage();
}catch(Exception $e){
	echo $e->getMessage();
}





