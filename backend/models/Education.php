<?php

    /**
    * Education.php
    * 
    * Education of person that attached to Resume
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package models;
    */

    namespace models;

    use system\classes\BaseModel;

    class Education extends BaseModel{
      
        protected $fields =['graduation_year','school_name','faculty'];
        
        function __construct(){
            parent::__construct();

        }
      
    }

