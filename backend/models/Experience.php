<?php

    /**
    * Experience.php
    * 
    * Experience of person that attached to Resume
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package models;
    */

    namespace models;

    use system\classes\BaseModel;

    class Experience extends BaseModel{
      
        protected $fields =['duration','date_range','role','company','location','industry','level','salary','job_description'];
        
        function __construct(){
            parent::__construct();

        }
      
    }

