<?php

    /**
    * Language.php
    * 
    * Language of person that attached to Resume
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package models;
    */

    namespace models;

    use system\classes\BaseModel;

    class Language extends BaseModel{
      
        protected $fields =['language_name','spoken','written'];
        
        function __construct(){
            parent::__construct();

        }
      
    }

