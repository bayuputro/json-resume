<?php

    /**
    * Profile.php
    * 
    * Profile of person that attached to Resume
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package models;
    */

    namespace models;

    use system\classes\BaseModel;

    class Profile extends BaseModel{
      
        protected $fields =['fullname','photo_url','current_role','current_company','phone_number','location','expected_salary','email','summary'];
        
        function __construct($fullname){
            parent::__construct();
            
            $this->flipped_fields['fullname'] = $fullname;
        }
      
    }

