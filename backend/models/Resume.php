<?php

    /**
    * Resume.php
    * 
    * Model to create and manage resume
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package models;
    */

    namespace models;

    use system\classes\BaseModel;

    class Resume extends BaseModel{
        
        protected $fields = ['profile','experiences','educations','languages','skills'];

        function __construct(){
            parent::__construct();
            
        }

        public function setProfile(Profile $profile){
            $this->flipped_fields['profile'] = $profile->toArray();
        }
        
        public function addExperience(Experience $Experience){
            $this->flipped_fields['experiences'][] = $Experience->toArray();
        }
        
        public function addEducation(Education $Education){
            $this->flipped_fields['educations'][] = $Education->toArray();
        }
        
        public function addLanguage(Language $Language){
            $this->flipped_fields['languages'][] = $Language->toArray();
        }
        
        public function addSkill(Skill $Skill){
            $this->flipped_fields['skills'][] = $Skill->toArray();
        }
    }

