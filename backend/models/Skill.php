<?php

    /**
    * Skill.php
    * 
    * Skill of person that attached to Resume
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package models;
    */

    namespace models;

    use system\classes\BaseModel;

    class Skill extends BaseModel{
      
        protected $fields =['skill_name','level'];
        
        function __construct(){
            parent::__construct();

        }
      
    }

