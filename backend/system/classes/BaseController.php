<?php
/**
 * system/BaseController.php
 * 
 * Base class for all controller
 * @author Bayu Putra <bayuputra_mail@yahoo.com>
 * @version 1.0
 * @package system\classes
 */
 
namespace system\classes;
 
abstract class BaseController{
	 function __construct(){
		
	 }
	 
	 protected function json($data){
		 echo json_encode($data);
	 }
 }