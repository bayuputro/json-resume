<?php
    /**
    * system/BaseModel.php
    * 
    * Base class for all model
    * @author Bayu Putra <bayuputra_mail@yahoo.com>
    * @version 1.0
    * @package system\classes
    */

    namespace system\classes;

    abstract class BaseModel{

        protected $fields = [];
        
        protected $flipped_fields = [];

        function __construct(){
            $this->_flip_fields();
        }

        public function toJson(){
            return json_decode($this->fields);
        }

        //setter of the field
        public function setField($key,$value){
            if(!$this->_field_exist($key))
                throw new \Exception("Invalid field name $key.");
            
            $this->flipped_fields[$key] = $value;
        }
        

        //getter of the field
        public function getField($key){
            
        }
        
        public function toArray(){
            return $this->flipped_fields;
        }
        
        public function printJson(){
            header('Content-type: application/json');
            echo json_encode($this->toArray());
        }
        
        private function _field_exist($field){
            return in_array($field,$this->fields);
        }
        
        private function _flip_fields()
        {
            $output = [];
            
            foreach($this->fields as $field){
                $output[$field] = null;    
            }
            
            $this->flipped_fields = $output;
        }
        

}