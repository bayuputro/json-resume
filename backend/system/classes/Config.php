<?php

/**
 * Config.php
 * 
 * Singleton to control config of application
 * @author Bayu Putra <bayuputra_mail@yahoo.com>
 * @version 1.0
 * @package system\classes
 */
 
 namespace system\classes;
 
 class Config{
	
	private $config;
	 
	private static $instance;
	public static function getInstance(){
		if(self::$instance==null)
			self::$instance = new Config();
		
		return self::$instance;
	}
	
	public function setFile($file){
		include $file;
		
		//transfering all config to private variable config
		foreach($config as $key => $value){
			$this->config[$key] = $value;
		}
	}
	
	public function get($key){
		return $this->config[$key];
	}
	
 }