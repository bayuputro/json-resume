<?php

/**
 * ControllerNotExistException.php
 * 
 * Exception that occure when controller is not found
 * @author Bayu Putra <bayuputra_mail@yahoo.com>
 * @version 1.0
 * @package 
 */
 
 namespace system\classes;
 
 class ControllerNotExistException extends \Exception{
	 function __construct(){
		 parent::__construct('Controller not found.');
	 }
 }
 