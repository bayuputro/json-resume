/**
* HTMLBuilder class is used for build HTML Element
* 
* @author Bayu Putra <bayuputra_mail@yahoo.com>
* @type Object
*/
var HTMLBuilder = {
    createList: function(style,data,callback){
        var ul = $('<ul class="'+style+'"></ul>');

        for(var index in data){
            li = callback(data[index]);
            ul.append(li);    
        }

        return ul;
    }
}