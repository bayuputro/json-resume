/**
* Configuration file
* 
* @author Bayu Putra <bayuputra_mail@yahoo.com>
* @type Object
*/

var config = {
    /**
    * Json data path to display 
    */
    rest_url:"../backend/index.php/bayu",
    
    /**
    * Timout time for give offset time after all json data loaded
    */
    loading_screen:{
        timeout: 1000
    }
}