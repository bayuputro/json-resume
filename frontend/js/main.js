/**
* Main Script
* 
* @author Bayu Putra <bayuputra_mail@yahoo.com>
*/

$(function(){

    var loading_screen = $('#loading_screen');

    //retreving JSON data
    $.getJSON( config.rest_url, function( resume ) {
        if(resume){
            //after get the resume data, hide the login screen
            setTimeout(function(){ loading_screen.fadeOut();}, config.loading_screen.timeout);

            //render the profile
            $('#fullname').html(resume.profile.fullname);
            $('#email').html(resume.profile.email);
            $('#current_role').html(resume.profile.current_role);
            $('#current_company').html(resume.profile.current_company);
            $('#phone_number').html(resume.profile.phone_number);
            $('#location').html(resume.profile.location);
            $('#expected_salary').html(resume.profile.expected_salary);
            $('#summary').html(resume.profile.summary);

            //render experinces
            $('#experiences').html(
                HTMLBuilder.createList('experiences',resume.experiences, function(item){
                    var li = $('<li/>');

                    container = $('<div/>');
                    container.append('<h2>'+item.company+' <small>'+item.industry+'</small></h2>');
                    container.append('<h3>'+item.role+' ('+item.duration+')</h3>');
                    container.append('<p>Job Description:</p>');

                    list = HTMLBuilder.createList('job_description', item.job_description, function(item){
                        var li = $('<li/>');
                        li.append('<p>'+item+'</p>')
                        return li;
                    })
                    
                    container.append(list);
                    li.append( container );

                    return li;
                })
            )

            //render educations
            $('#educations').html( 
               HTMLBuilder.createList('educations',resume.educations, function(item){
                    li = $('<li>' + item.school_name + ' (Graduation '+item.graduation_year+')</li>');                                                
                    return li;
                })
            );
            
            //render language
            $('#languages').html( 
                HTMLBuilder.createList('languages',resume.languages, function(item){
                    li = $('<li>' + item.language_name + '</li>');                                                
                    return li;
                })
            );
            
            //render language
            $('#skills').html( 
                HTMLBuilder.createList('skills',resume.skills, function(item){
                    li = $('<li>' + item.skill_name + ' ('+item.level+')</li>');                                                
                    return li;
                })
            );

        }
    });              

    


});